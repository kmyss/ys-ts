import { ysRead } from "@ys-lib/ts";

async function read() {
  let msg = await ysRead.ReadlineUtils.ysReadline("第一次输入:");
  console.log(msg);

  msg = await ysRead.ReadlineUtils.ysReadline("第二次输入:");
  console.log(msg);

  ysRead.ReadlineUtils.ysReadlineClose();
}

read();
