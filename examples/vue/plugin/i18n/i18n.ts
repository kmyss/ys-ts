import { createI18n as base } from "vue-i18n";

export function createI18n() {
  return base<{ messages: any }>({
    legacy: false,
    messages: {
      "en-US": {},
    },
  });
}
