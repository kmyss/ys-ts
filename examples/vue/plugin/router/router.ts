import { useAppRouter, useAppRouterName } from "./app";
import { RouterUtil } from "@ys-lib/vue/router";

export function useRouterName() {
  return {
    ...useAppRouterName(),
  };
}

function initRoute() {
  console.log(`[router] init`);

  const routerUtil = new RouterUtil();

  routerUtil.addConfig(useAppRouter().build());
  return routerUtil;
}

export function createRouter() {
  return initRoute().build();
}
