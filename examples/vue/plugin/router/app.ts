import { YsRouteRecord } from "@ys-lib/vue/router";
import Index from "../../components/layout/LayoutIndex.vue";
import Home from "../../components/pages/HomePage.vue";

import TSInstanceExample from "../../components/pages/ts/InstanceExample.vue";

import VueMQWebSocket from "../../components/pages/vue/MQWebSocket.vue";

import NaiveUIIconExample from "../../components/pages/naive-ui/iconExample.vue";
import NaiveUIYSLogExample from "../../components/pages/naive-ui/YSLogExample.vue";
import NaiveUIRouterExample from "../../components/pages/naive-ui/RouterExample.vue";

import NaiveUIDemoCarousel from "../../components/pages/naive-ui-demo/Carousel.vue";

import DrawMermaidBase from "../../components/pages/draw/MermaidBaseExample.vue";
import DrawMonacoExample from "../../components/pages/draw/MonacoExample.vue";

import { getLogger } from "@ys-lib/ts/log";
import { defaultProps } from "../../../../src/naive-ui/router";

export function useAppRouterName() {
  return {
    app: "app",
    home: "home",
    tsInstance: "tsInstance",
    vueMQWebSocket: "vueMQWebSocket",
    naiveUIIcon: "naiveUIIcon",
    naiveUIYSLog: "naiveUIYSLog",
    naiveUIRouter: "naiveUIRouter",
    naiveUIDemoCarousel: "naiveUIDemoCarousel",
    drawMermaidBase: "drawMermaidBase",
    drawMonaco: "drawMonaco",
  };
}

export function useAppRouter() {
  const logger = getLogger("plugin.router.app");
  const name = useAppRouterName();

  const cfgApp = new YsRouteRecord({
    path: "/",
    name: name.app,
    component: Index,
    meta: {
      useTab: true,
    },
  });

  const cfgHome = new YsRouteRecord({
    path: "",
    name: name.home,
    component: Home,
  });
  cfgApp.addChildren(cfgHome);

  const cfgTSInstance = new YsRouteRecord({
    path: "ts/instance",
    name: name.tsInstance,
    component: TSInstanceExample,
  });
  cfgApp.addChildren(cfgTSInstance);

  const cfgVueMQWebSocket = new YsRouteRecord({
    path: "vue/vue-mq-web-socket",
    name: name.vueMQWebSocket,
    component: VueMQWebSocket,
  });
  cfgApp.addChildren(cfgVueMQWebSocket);

  const cfgNaiveUIIcon = new YsRouteRecord({
    path: "naive-ui/icon",
    name: name.naiveUIIcon,
    component: NaiveUIIconExample,
  });
  cfgApp.addChildren(cfgNaiveUIIcon);

  const cfgNaiveUIYSLog = new YsRouteRecord({
    path: "naive-ui/ys-log",
    name: name.naiveUIYSLog,
    component: NaiveUIYSLogExample,
  });
  cfgApp.addChildren(cfgNaiveUIYSLog);

  const cfgNaiveUIRouter = new YsRouteRecord({
    path: "naive-ui/router",
    name: name.naiveUIRouter,
    component: NaiveUIRouterExample,
    props: defaultProps,
  });
  cfgApp.addChildren(cfgNaiveUIRouter);

  const cfgNaiveUIDemoCarousel = new YsRouteRecord({
    path: "naive-ui-demo/carousel",
    name: name.naiveUIDemoCarousel,
    component: NaiveUIDemoCarousel,
  });
  cfgApp.addChildren(cfgNaiveUIDemoCarousel);

  const cfgDrawMermaidBase = new YsRouteRecord({
    path: "draw/mermaid-base",
    name: name.drawMermaidBase,
    component: DrawMermaidBase,
  });
  cfgApp.addChildren(cfgDrawMermaidBase);

  const cfgDrawMonaco = new YsRouteRecord({
    path: "draw/mo",
    name: name.drawMonaco,
    component: DrawMonacoExample,
  });
  cfgApp.addChildren(cfgDrawMonaco);

  return cfgApp;
}
