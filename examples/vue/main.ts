import { createApp } from "vue";
import App from "./App.vue";
import { initWindowDebug, _d } from "@ys-lib/ts/log";
import { createRouter } from "./plugin/router";
import "./style/index.css";
import { createPinia } from "pinia";
import { initIconMap } from "@ys-lib/naive-ui/icon";
import { createI18n } from "./plugin/i18n/i18n";
import "@ys-lib/draw/initMonacoWorker";
import naive from "naive-ui";

if (import.meta.env.MODE === "lib") {
  import("@ys-lib/naive-ui/other/useYsLog.css");
}

async function init() {
  // 初始化内部logger
  initWindowDebug();
  _d.enable("*");

  //初始化图标
  initIconMap();

  // 初始化 MQ WebSocket

  const app = createApp(App);

  app.use(naive);
  app.use(createI18n());
  app.use(createRouter());
  app.use(createPinia());

  app.mount("#app");
}

init();
