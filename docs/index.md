---
layout: home

title: ys-ts
titleTemplate: yss 个人积累的一套 ts 组件库

hero:
  name: ys-ts
  text: 一个杂乱的组件库
  tagline: 仅供个人使用没啥特点
  actions:
    - theme: brand
      text: 开始
      link: /guide/
    - theme: alt
      text: 在 Gitee 上查看
      link: https://gitee.com/kmyss/ys-ts.git

features:
  - icon: 💡
    title: Typescript 组件库
    details: 基于vite打包的组件库，里面还依赖好多好多别人的库如 debug、axios、vue 等等
  - icon: 📦
    title: 仅供个人使用
    details: 这是我个人使用的项目库，不要用于你们的生成环境哦~
  - icon: 🛠️
    title: 仅供学习使用
    details: 想学习库打包的可以看看这个库~
---


