import { defineConfig } from "vitepress";

export default defineConfig({
  themeConfig: {
    nav: [
      { text: "指南", link: "/guild/installation" },
      { text: "组件", link: "/examples/" },
    ],
    socialLinks: [{ icon: "github", link: "https://gitee.com/kmyss" }],
  },
});
