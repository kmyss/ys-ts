/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    "./examples/vue/**/*.{vue,js,ts,jsx,tsx}",
  ],
  presets: [require("./tailwind.ysTs.config.js")],
};
