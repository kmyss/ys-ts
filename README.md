# 概述

这是一个沉淀了很久的代码库。功能繁多就在使用中慢慢看吧。
此库尝试使用 vite 打包哦。不同的库使用到的依赖都不相同，请在源代码中查找。 

使用此库目前需要添加为 vite 添加编译参数，include 根据调用到的库的不同增加或减少，具体增加或减少的需要参看使用的包包括什么三方库
```ts
export default defineConfig({
  // ...
  optimizeDeps: {
    include: [
      "debug",
      "axios",
      "lodash",
      "yaml",
      "events",
      "lodash",
      "@vicons/fluent",
      "pinia",
      "naive-ui",
      "vue-i18n",
      "vue-router",
      "vue",
    ],
    exclude: ["ys-ts"],
  },
  // ...
});
```