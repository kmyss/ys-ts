import { describe, it, expect } from "vitest";
import * as LodashUtil from "@ys-lib/ts/lodash";
import * as lodash from "lodash";

describe("正常测试", () => {
  it("searchObject-object", () => {
    const obj = {
      name: "test",
      age: 20,
      isMan: false,
    };
    expect(LodashUtil.searchObject("name", obj)).toBe("test");
    expect(LodashUtil.searchObject("age", obj)).toBe(20);
    expect(LodashUtil.searchObject("isMan", obj)).toBeFalsy();
    expect(LodashUtil.searchObject("test", obj, false)).toBeFalsy();
    expect(LodashUtil.searchObject("test", obj)).toBeUndefined();
  });

  it("searchObject-array", () => {
    const obj = [{ name: "test", age: 20, isMan: false }];
    expect(LodashUtil.searchObject("[0].name", obj)).toBe("test");
    expect(LodashUtil.searchObject("[0].age", obj)).toBe(20);
    expect(LodashUtil.searchObject("[0].isMan", obj)).toBeFalsy();
    expect(LodashUtil.searchObject("[1]", obj, false)).toBeFalsy();
    expect(LodashUtil.searchObject("[1]", obj)).toBeUndefined();
  });

  it("isEmpty", () => {
    expect(LodashUtil.isEmpty(undefined)).toBeTruthy();
    expect(LodashUtil.isEmpty(0)).toBeTruthy();
    expect(LodashUtil.isEmpty("")).toBeTruthy();
    expect(LodashUtil.isEmpty([])).toBeTruthy();
    expect(LodashUtil.isEmpty({})).toBeTruthy();
    expect(LodashUtil.isEmpty(null)).toBeTruthy();
    expect(LodashUtil.isEmpty(false)).toBeTruthy();
    expect(LodashUtil.isEmpty(true)).toBeTruthy();

    expect(LodashUtil.isEmpty([1])).toBeFalsy();
    expect(LodashUtil.isEmpty({ a: 1 })).toBeFalsy();

    const obj = { a: [], b: 0, c: "", d: false };

    expect(LodashUtil.isEmpty(obj.a)).toBeTruthy();
    expect(LodashUtil.isEmpty(obj.b)).toBeTruthy();
    expect(LodashUtil.isEmpty(obj.c)).toBeTruthy();
    expect(LodashUtil.isEmpty(obj.d)).toBeTruthy();
  });

  it("arrayRanges-只设置 length", () => {
    const arrayRanges = LodashUtil.arrayRanges({
      length: 10,
    });
    expect(arrayRanges.length).toBe(10);
    expect(arrayRanges).toStrictEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
  });

  it("mapKeys", () => {
    const keys = ["1", "2", "3", "4"];
    const map = new Map<string, number>();
    map.set("1", 0);
    map.set("2", 0);
    map.set("3", 0);
    map.set("4", 0);

    expect(LodashUtil.mapKeys(map)).toStrictEqual(keys);
    expect(LodashUtil.mapKeys(map)).not.toStrictEqual(["4", "2", "3", "1"]);
  });

  it("pathJion", () => {
    expect(LodashUtil.pathJoin(["D:\\test", "../AIS"])).toStrictEqual("D:/AIS");
    expect(LodashUtil.pathJoin(["D:\\test\\test", "AIS"])).toStrictEqual(
      "D:/test/test/AIS"
    );
  });
});

describe("lodash使用测试", () => {
  it("Map 获取 keys", () => {
    const keys = ["1", "2", "3", "4"];
    const map = new Map<string, number>();
    map.set("1", 0);
    map.set("2", 0);
    map.set("3", 0);
    map.set("4", 0);

    console.log(lodash.keys(map));

    expect(map.keys().next()).not.toStrictEqual(keys);
  });
});
