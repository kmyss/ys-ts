import { describe, it, expect } from "vitest";
import * as CryptoUtil from "@ys-lib/ts/crypto";

describe("正常测试", () => {
  it("MD5-Hello", () => {
    expect(CryptoUtil.md5("Hello")).eq("8b1a9953c4611296a827abf8c47804d7");
  });

  it("MD5-HelloWorld", () => {
    expect(CryptoUtil.md5("HelloWorld")).eq("68e109f0f40ca72a15e05cc22786f8e6");
  });

  it("base64-jwt", () => {
    expect(
      CryptoUtil.decode(
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
      )
    ).toStrictEqual({
      sub: "1234567890",
      name: "John Doe",
      iat: 1516239022,
    });
  });
});

describe("异常测试", () => {
  it("MD5-Hello1", () => {
    expect(CryptoUtil.md5("Hello1")).not.eq("8b1a9953c4611296a827abf8c47804d7");
  });

  it("MD5-HelloWorld2", () => {
    expect(CryptoUtil.md5("HelloWorld2")).not.eq(
      "68e109f0f40ca72a15e05cc22786f8e6"
    );
  });

  it("decode-jwt-非token", () => {
    expect(CryptoUtil.decode("1234aksjdhfkh")).toBe("给的数据不是 JWT");
  });

  it("decode-jwt-错误的base64", () => {
    expect(CryptoUtil.decode("1234aksjdhfkh.1231231.1231231")).toBe(
      "payload 异常"
    );
  });
});
