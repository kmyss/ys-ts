import { describe, it, expect } from "vitest";
import { Instance } from "@ys-lib/ts/instance";

describe("单例正常测试", () => {
  const key = "test";
  const value = "test";

  it("set", () => {
    Instance.Instance().set(key, value);
  });

  it("get", () => {
    const v = Instance.Instance().get(key);
    expect(v).toBe(value);
  });
});
