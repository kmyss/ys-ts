import { describe, it, expect } from "vitest";
import { AxiosUtil } from "@ys-lib/ts/axios";

describe("axios 封装测试", () => {
  it("get", async () => {
    const axios = AxiosUtil.newAxios({});
    const res = await axios.get("http://127.0.0.1:5173/hello.json");
    expect(res.status).toBe(200);
    expect(res.data).toStrictEqual({
      hello: "world",
    });
  });
});
