import { describe, it, expect } from "vitest";
import { PromiseUtil } from "@ys-lib/ts/promise";

describe("正常使用", () => {
  it("拒绝辅助函数", async () => {
    try {
      await PromiseUtil.error("test");
      // 不应该运行到这个
      expect(false).toBeTruthy();
    } catch (err) {
      expect(err).toBe("test");
    }
  });
});
