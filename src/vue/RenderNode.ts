import { defineComponent, h } from "vue";
import { getLogger } from "../ts/log";

export const RenderNode = defineComponent({
  props: {
    node: null,
  },
  setup(props: { node: any }) {
    const logger = getLogger("ys-ts.vue.renderNode");

    logger("渲染的节点", props.node);

    return () => h(props.node());
  },
});
