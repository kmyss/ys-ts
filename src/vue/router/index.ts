import {
  createRouter,
  Router,
  RouteRecordRaw,
  RouterOptions,
  createWebHashHistory,
  NavigationGuardWithThis,
  NavigationHookAfter,
} from "vue-router";
import { getLogger } from "../../ts/log";

export class YsRouteRecord {
  private readonly cfg: RouteRecordRaw;
  private children: YsRouteRecord[];

  constructor(cfg: RouteRecordRaw) {
    this.cfg = cfg;
    this.children = [];
  }

  addChildren(record: YsRouteRecord): YsRouteRecord {
    this.children.push(record);
    return this;
  }

  build(): RouteRecordRaw {
    if (this.children.length > 0) {
      const childrenCfg: RouteRecordRaw[] = [];
      this.children.forEach((value) => {
        childrenCfg.push(value.build());
      });

      this.cfg.children = childrenCfg;
    }
    return this.cfg;
  }
}

export class RouterUtil {
  private readonly cfgs: Array<RouteRecordRaw>;
  private beforeEach: NavigationGuardWithThis<any>[];
  private beforeResolve: NavigationGuardWithThis<any>[];
  private afterEach: NavigationHookAfter[];
  private logger = getLogger("ys-ts.vue.router.RouterUtil");

  constructor() {
    this.cfgs = [];
    this.beforeEach = [];
    this.afterEach = [];
    this.beforeResolve = [];
  }

  addBeforeEach<T = any>(before: NavigationGuardWithThis<T>): RouterUtil {
    this.beforeEach.push(before);
    return this;
  }

  addBeforeResolve<T = any>(before: NavigationGuardWithThis<T>): RouterUtil {
    this.beforeResolve.push(before);
    return this;
  }

  addAfterEach(after: NavigationHookAfter): RouterUtil {
    this.afterEach.push(after);
    return this;
  }

  addConfig(cfg: RouteRecordRaw): RouterUtil {
    this.cfgs.push(cfg);
    return this;
  }

  build(options: Partial<RouterOptions> = {}): Router {
    this.logger(`[RouterTool]`, this.cfgs);
    const router = createRouter({
      history: createWebHashHistory(),
      routes: this.cfgs,
      ...options,
    });

    this.beforeEach.forEach((value) => {
      router.beforeEach(value);
    });

    this.beforeResolve.forEach((value) => {
      router.beforeResolve(value);
    });

    this.afterEach.forEach((value) => {
      router.afterEach(value);
    });

    return router;
  }
}
