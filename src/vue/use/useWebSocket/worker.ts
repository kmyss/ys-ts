import { WebWorkerBase } from "../../../ts/webworker";
import { doUnPack, MQWorkerMessageType } from "./common";
import * as lodash from "lodash";
import { isEmpty } from "../../../ts/lodash";

interface WorkerMQWebSocketConfigInternal {
  // websocket 服务路径
  url: string;
}

class MQWorker extends WebWorkerBase {
  // websocket 连接对象
  private ws: WebSocket | undefined;
  // 配置参数
  private timer: NodeJS.Timeout | undefined;
  private topics = "";
  private id = 0;

  constructor() {
    super();
    console.log("构造ws对象");
  }

  init() {
    super.init();

    this.addHandler(MQWorkerMessageType.OPEN, (type, data) => {
      this.logger("启动连接");
      this.doConnect();
    });
    this.addHandler(MQWorkerMessageType.SUB, (type, data) => {
      this.logger(`订阅`, data);
      this.sub(data.topics);
    });
    this.addHandler(MQWorkerMessageType.PUB, (type, data) => {
      this.logger(`发布`, data);
      this.pub(data.topic, data.message);
    });
  }

  private doConnect() {
    this.ws = new WebSocket(this.cfg.url);

    // 注意这里使用 lambda 表达式让函数内的 this 指向正确的对象
    this.ws.onclose = () => {
      this.onWSClose();
    };
    this.ws.onopen = () => {
      this.onWSOpen();
    };
    this.ws.onmessage = (result: MessageEvent) => {
      this.onWSMessage(result);
    };
  }

  private onWSClose() {
    this.ws = undefined;
    this.postMessage({ type: MQWorkerMessageType.CLOSE, data: {} });
    this.logger(`连接已关闭`);
    this.timer = setTimeout(() => {
      this.doConnect();
    }, 3000);
  }

  private isConnected() {
    return this.ws != undefined;
  }

  private onWSOpen() {
    this.postMessage({ type: MQWorkerMessageType.OPEN, data: {} });
    console.log(`连接已开启`);
  }

  private onWSMessage(result: MessageEvent) {
    let data = JSON.parse(result.data);
    if (!data.message || !data.topic) {
      this.logger(`WebSocket获取的 data存在问题`, result.data);
    }

    this.logger(`接收话题: `, data.topic);

    // 判断 topic 是否存在分包现象
    data = doUnPack(data);
    if (data == null) {
      this.logger(`分包数据没有处理`);
      return;
    }

    const transType = lodash.split(data.topic, ".")[0];
    let msg: any = null;
    switch (transType) {
      case "json":
        msg = JSON.parse(data.message);
        break;
      case "ok":
      case "error":
      case "str":
        msg = data.message;
        break;
      default:
        console.error(`不能解析 ${transType} 类数据`);
        return;
    }

    // 对于 ok.sub 主题处理
    if (data.topic === "ok.sub") {
      this.id = JSON.parse(data.message).id;
      this.logger("订阅ID：", this.id);
    }

    // 数据处理完成后通过 post发送结果
    this.postMessage({
      type: MQWorkerMessageType.MESSAGE,
      data: {
        topic: data.topic,
        msg: msg,
      },
    });
  }

  private async unSub() {
    this.topics = "";
    this.ws!.send(
      JSON.stringify({
        id: this.id,
      })
    );
    this.id = 0;
  }

  private sub(topics: string): boolean {
    if (!this.isConnected()) {
      this.logger(`未连接无法订阅`);
      return false;
    }

    if (!isEmpty(this.id)) {
      this.unSub();
    }

    this.topics = topics;

    this.ws!.send(
      JSON.stringify({
        topics: topics,
      })
    );

    return true;
  }

  private async pub(topic: string, message: string) {
    this.logger(`pub`, topic);
    // this.logger(`[mqWebsocket] message: ${message}`);

    // 分包处理
    const size = 1024 * 1024 * 3;
    if (message.length > size) {
      const packs = Math.ceil(message.length / size);
      this.logger(`消息长度：${message.length} 分包数：${packs}`);

      const msgs: string[] = [];
      for (let i = 0; i < packs; i++) {
        msgs.push(message.slice(i * size, i * size + size));
      }

      // 发送分包数据
      for (let i = 0; i < msgs.length; i++) {
        const pack_topic = `${topic}[${i}-${packs}]`;
        this.logger(`发送包 ${pack_topic}`);
        setTimeout(() => {
          this.ws?.send(
            JSON.stringify({
              topic: pack_topic,
              message: msgs[i],
            })
          );
        }, 10);
      }
    } else {
      this.ws?.send(
        JSON.stringify({
          topic,
          message,
        })
      );
    }
  }
}

const worker = new MQWorker();
worker.init();
