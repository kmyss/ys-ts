import * as lodash from "lodash-es";

export enum MQ_TOPICS {
  HEART = "json.heart", // 心跳话题
  REQ = "json.req", // 请求话题
  RES = "json.res", // 响应话题

  OK = "json.ok",
  CFG = "json.cfg",
  DATA = "json.data",
  DATA_RAW = "json.data.raw",

  LOG = "str.log",
}

export class MQTopicUtils {
  /**
   * 生成符合 MQ 协议的话题
   * @param base 基础标识
   * @param model 模块名
   * @param ex 扩展标识
   * @param separator 中间的分割符号
   */
  public static genTopic(
    base: MQ_TOPICS,
    model: string,
    ex = "",
    separator = "."
  ) {
    const s = [base, model];
    if (!lodash.isEmpty(ex)) {
      s.push(ex);
    }
    return lodash.join(s, separator);
  }

  public static genHeartAsk() {
    return MQ_TOPICS.HEART + "?";
  }

  public static genHeartTopic(model: string, ex = "") {
    return MQTopicUtils.genTopic(MQ_TOPICS.HEART, model, ex);
  }

  public static genReqTopic(model: string, ex = "") {
    return MQTopicUtils.genTopic(MQ_TOPICS.REQ, model, ex);
  }

  public static genResTopic(model: string, ex = "") {
    return MQTopicUtils.genTopic(MQ_TOPICS.RES, model, ex);
  }

  public static genDataTopic(model: string, ex = "") {
    return MQTopicUtils.genTopic(MQ_TOPICS.DATA, model, ex);
  }

  public static genDataRawTopic(model: string, ex = "") {
    return MQTopicUtils.genTopic(MQ_TOPICS.DATA_RAW, model, ex);
  }

  public static genLogTopic(model: string, ex = "") {
    return MQTopicUtils.genTopic(MQ_TOPICS.LOG, model, ex);
  }

  public static genCfg(model = "__all") {
    return MQTopicUtils.genTopic(MQ_TOPICS.CFG, model, "");
  }

  public static genCfgAsk(model = "") {
    return MQ_TOPICS.CFG + "?" + model;
  }
}

export enum COMMON_MODEL_NAME {
  FILE = "file",
  CONFIG = "config",
}

export const COMMON_TOPICS = new (class {
  ASK_HEART = MQTopicUtils.genHeartAsk(); // 主程序询问心跳

  // 文件管理模块
  FILE_REQ_RECORD = MQTopicUtils.genReqTopic(COMMON_MODEL_NAME.FILE, "record");
  FILE_REQ_READ_RECORD = MQTopicUtils.genReqTopic(
    COMMON_MODEL_NAME.FILE,
    "read-record"
  );
  FILE_RES_READ_RECORD = MQTopicUtils.genResTopic(
    COMMON_MODEL_NAME.FILE,
    "read-record"
  );
  FILE_REQ_LIST = MQTopicUtils.genReqTopic(COMMON_MODEL_NAME.FILE, "list");
  FILE_RES_LIST = MQTopicUtils.genResTopic(COMMON_MODEL_NAME.FILE, "list");
  FILE_REQ_WRITE_FILE = MQTopicUtils.genReqTopic(
    COMMON_MODEL_NAME.FILE,
    "write-file"
  );
  FILE_RES_WRITE_FILE = MQTopicUtils.genResTopic(
    COMMON_MODEL_NAME.FILE,
    "write-file"
  );
  FILE_REQ_READ_FILE = MQTopicUtils.genReqTopic(
    COMMON_MODEL_NAME.FILE,
    "read-file"
  );
  FILE_RES_READ_FILE = MQTopicUtils.genResTopic(
    COMMON_MODEL_NAME.FILE,
    "read-file"
  );

  // 配置中心
  CONFIG_REQ_UPDATE = MQTopicUtils.genReqTopic(
    COMMON_MODEL_NAME.CONFIG,
    "update"
  );
  CONFIG_RES_UPDATE = MQTopicUtils.genResTopic(
    COMMON_MODEL_NAME.CONFIG,
    "update"
  );
})();
