import { defineStore } from "pinia";
import { getLogger } from "../../../ts/log";
import { MQTopicUtils, useMqWebSocket } from "./index";
import { Instance } from "../../../ts/instance";
import { isEmpty } from "../../../ts/lodash";
import * as lodash from "lodash";

const key = () => "config.mq";

export interface IConfigSchemaSplitOptions {
  path?: string; // 节点路径
  splits?: number; // 要分割的个数默认分隔分数为 2
  type?: "interval" | "continuity"; // 间隔模式 和 连续模式 默认是 间隔模式
}

const logger = getLogger("ys-ts.vue.use.mq-config");

/**
 * 使用 mq 读取的到的配置数据
 * 注意在使用过程中 需要使用 $state 监控
 */
export const useMQCfgStore = <T = any>() => {
  const store = defineStore({
    // 它用于 devtools 并允许恢复状态
    id: "mqCfg",
    state: (): Partial<T> => {
      const cfg = Instance.Instance().get(key());
      logger("state", cfg);
      return cfg ? (lodash.cloneDeep(cfg) as T) : {};
    },
    actions: {
      update() {
        const { ws } = useMqWebSocket({});
        ws.pubJson(MQTopicUtils.genReqTopic("config", "update"), this.$state);
      },

      cfgSchemaSplit(
        options: IConfigSchemaSplitOptions
      ): Array<Array<any>> | null {
        const { path = "cfgSchemas", splits = 2, type = "interval" } = options;

        // 判断是否存在指定的路径
        const result = lodash.get(this.$state, path, null);
        if (isEmpty(result)) {
          logger(`指定路径不存在 ${path}`);
          return null;
        }

        // 获取排序
        let order = lodash.get(result, "_order", null);
        if (isEmpty(order)) {
          order = lodash.keys(result);
        }

        // 构建输出数组
        const out: Array<Array<any>> = [];
        for (let i = 0; i < splits; i++) {
          out.push([]);
        }

        // 生成分隔数据
        logger(`${type}`);
        switch (type) {
          case "interval": {
            order.forEach((key: string, index: number) => {
              logger(`key: ${key}, index: ${index}, ${index % splits}`);
              out[index % splits].push(lodash.get(result, key, null));
            });
            break;
          }
          case "continuity": {
            const length = order.length / splits;
            order.forEach((key: string, index: number) => {
              logger(
                `key: ${key}, index: ${index}, ${Math.floor(index / length)}`
              );
              out[Math.floor(index / length)].push(
                lodash.get(result, key, null)
              );
            });
            break;
          }
        }
        logger(`最终分隔输出：`, out);
        return out;
      },
    },
  });

  return store();
};

/**
 * 提前加载配置数据
 */
export async function loadMQConfig() {
  const data: null | Record<string, any> = null;
  const { ws, doOnConnect } = useMqWebSocket({});

  // 模型名称web
  const modelName = "web";
  /**
   * 订阅配置管理参数
   * @param topic
   * @param msg
   */
  const resetCfg = (topic: string, msg: any) => {
    // 先删除原始的，再更新现在的
    Instance.Instance().remove(key());
    Instance.Instance().set<Record<string, any>>(key(), msg);
    useMQCfgStore().$reset();
    logger("更新配置参数");
  };

  ws.addTopics([MQTopicUtils.genCfg(), MQTopicUtils.genCfg(modelName)]);
  // 订阅 __all 情况和 web 情况下的配置参数
  ws.addMQHandler(MQTopicUtils.genCfg(), resetCfg);
  ws.addMQHandler(MQTopicUtils.genCfg(modelName), resetCfg);

  // 在 MQ 连接后会主动获取一次配置
  doOnConnect(() => {
    setTimeout(() => {
      ws.pubJson("json.cfg?web", {});
    }, 2000);
  });
}
