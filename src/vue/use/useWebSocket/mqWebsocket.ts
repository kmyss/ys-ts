import * as lodash from "lodash-es";
import { Ref } from "vue";
import MQWorker from "./worker?worker";
import { NormalMessageType, WebWorkerStartBase } from "../../../ts/webworker";
import { MQWorkerMessageType } from "./common";
import { mapKeys } from "../../../ts/lodash";

export type MQHandler = (topic: string, message: any) => void;

export interface MQWebSocketConfig {
  // websocket 服务路径
  url: string;
}

export function NewMQWebSocketConfig(
  cfg: Partial<MQWebSocketConfig>
): MQWebSocketConfig {
  return {
    url: "ws://127.0.0.1",
    ...cfg,
  };
}

interface MQWebSocketConfigInternal {
  // websocket 服务路径
  url: string;

  // 外部绑定的 Connect 属性
  isConnected: Ref<boolean> | undefined;
}

export class MQWebSocket extends WebWorkerStartBase {
  // topic 相关处理函数
  private mqHandlers: Map<string, MQHandler>;
  private _dispose = false;
  private timer: NodeJS.Timeout | undefined;
  private topics: Map<string, number> = new Map<string, number>();

  constructor(cfg: MQWebSocketConfigInternal) {
    super();
    this.name = "MQ";
    this.cfg = cfg;
    this.mqHandlers = new Map();
    this.startWorker(new MQWorker());
  }

  init() {
    super.init();
    const start = this.getHandler(NormalMessageType.STARTED)!;
    this.addHandler(NormalMessageType.STARTED, () => {
      start(0, 0);

      this.logger("执行 启动handler");
      this.logger(this);

      this.postMessage({
        type: NormalMessageType.CFG,
        data: {
          url: this.cfg.url,
        },
      });

      this.postMessage({
        type: MQWorkerMessageType.OPEN,
        data: null,
      });
    });

    this.addHandler(MQWorkerMessageType.MESSAGE, (type, data) => {
      this.messageHandler(type, data);
    });
    this.addHandler(MQWorkerMessageType.CLOSE, (type, data) => {
      this.closeHandler();
    });
    this.addHandler(MQWorkerMessageType.OPEN, (type, data) => {
      this.openHandler();
    });
  }

  private closeHandler() {
    this.changeIsConnected(false);
    this.logger(`连接已关闭`);
  }

  private changeIsConnected(value: boolean) {
    if (this.cfg.isConnected) {
      this.cfg.isConnected.value = value;
    }
  }

  private openHandler() {
    this.changeIsConnected(true);
    this.logger(`连接已开启`);
  }

  private messageHandler(type: number | string, data: any) {
    const topic = data.topic;
    const msg = data.msg;
    this.mqHandlers.forEach((value, key) => {
      if (lodash.startsWith(topic, key)) {
        this.logger(`处理消息 ${topic}`);
        value(topic, msg);
      }
    });
  }

  /**
   * 增加要订阅的一组话题
   * @param topics 话题字符串
   */
  public addTopics(topics: string[]) {
    topics.forEach((topic) => {
      this.addTopic(topic);
    });
  }

  /**
   * 增加要订阅的一个话题
   * @param topic 话题字符串
   */
  public addTopic(topic: string) {
    this.topics.set(topic, 0);
    this.doSub();
  }

  /**
   * 删除订阅的一组话题
   * @param topics 话题组
   */
  public removeTopics(topics: string[]) {
    topics.forEach((topic) => {
      this.removeTopic(topic);
    });
  }

  /**
   * 删除订阅的一个话题
   * @param topic 话题字符串
   */
  public removeTopic(topic: string) {
    this.topics.delete(topic);
    this.doSub();
  }

  public doSub() {
    if (this.cfg.isConnected && this.cfg.isConnected.value) {
      this.clearTimer();
      this.timer = setTimeout(() => {
        this.sub(lodash.join(mapKeys(this.topics), ","));
      }, 1000);
    }
  }

  private sub(topics: string): boolean {
    this.postMessage({
      type: MQWorkerMessageType.SUB,
      data: {
        topics: topics,
      },
    });
    return true;
  }

  public pub(topic: string, message: string) {
    this.postMessage({
      type: MQWorkerMessageType.PUB,
      data: {
        topic: topic,
        message: message,
      },
    });
  }

  public pubJson(topic: string, message: any) {
    this.pub(topic, JSON.stringify(message));
  }

  /**
   * 销毁 ws
   * 在内部已调用,不用自己调用此函数
   */
  dispose() {
    this._dispose = true;
    this.worker && this.worker.terminate();
    this.clearTimer();
  }

  private clearTimer() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  public addMQHandler(topic: string, func: MQHandler) {
    this.logger(`增加处理函数`, topic);
    this.mqHandlers.set(topic, func);
  }

  public removeMQHandler(topic: string) {
    this.logger(`删除处理函数`, topic);
    this.mqHandlers.delete(topic);
  }
}
