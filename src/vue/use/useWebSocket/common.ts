import { getLogger } from "../../../ts/log";
import { Instance } from "../../../ts/instance";
import { add, isBefore } from "date-fns";
import * as lodash from "lodash";

export enum MQWorkerMessageType {
  CLOSE = 1000,
  OPEN,
  MESSAGE,
  PUB,
  SUB,
  DISPOSE,
}

interface MQWebSocketPack {
  topic: string;
  packs: number;
  msgs: { index: number; msg: string }[];
  time: Date;
}

export function doUnPack(data: any) {
  const logger = getLogger("ys-ts.mqUnpack");

  const packBuf = Instance.Instance().getOrSetFunc<
    Map<string, MQWebSocketPack>
  >("mq.unpack", () => {
    return new Map<string, MQWebSocketPack>();
  });

  const re = /(.*)\[([0-9]+)-([0-9]+)]$/;
  if (!re.test(data.topic)) {
    return data;
  }

  const match = re.exec(data.topic)!;
  const topic = match[1];
  const index = match[2];
  const packs = match[3];

  // 10秒内没有来包就删除
  if (packBuf.has(topic)) {
    if (isBefore(packBuf.get(topic)!.time, Date.now())) {
      logger(`超时 ${topic}`);
      packBuf.delete(topic);
    }
  }

  if (!packBuf.has(topic)) {
    packBuf.set(topic, {
      topic,
      packs: Number(packs),
      msgs: [],
      time: add(Date.now(), {
        seconds: 10,
      }),
    });
  }

  logger(`正在处理 ${topic}-${index}`);

  const pack = packBuf.get(topic)!;
  pack.msgs.push({
    index: Number(index),
    msg: data.message,
  });

  if (pack.msgs.length >= pack.packs) {
    logger(`正在拼接数据 ${topic}`);
    const n = lodash.sortBy(pack.msgs, ["index"]);
    let message = "";
    n.forEach((value) => {
      message += value.msg;
    });
    packBuf.delete(topic);
    return {
      topic: topic,
      message: message,
    };
  }

  return null;
}
