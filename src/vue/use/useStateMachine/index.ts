import { ref } from "vue";

export interface StateMachineOption {
  /**
   * 状态机最大状态值
   *
   * @default 0
   */
  max?: number;
}

export function useStateMachine(options: StateMachineOption) {
  const { max = 0 } = options;

  const state = ref(0);

  const next = () => {
    if (state.value < max) {
      state.value += 1;
    }
  };

  const before = () => {
    if (state.value > 0) {
      state.value -= 1;
    }
  };
  const first = () => {
    state.value = 0;
  };
  const last = () => {
    state.value = max;
  };

  return {
    state,
    next,
    before,
    first,
    last,
  };
}
