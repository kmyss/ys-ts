import { ref } from "vue";

/**
 * 延时 loading 处理机制
 * @param time 延时时间
 */
export const useLoading = (time = 500) => {
  const loading = ref(true);

  const delayLoadingClose = () => {
    setTimeout(() => {
      loading.value = false;
    }, time);
  };

  return {
    loading,
    delayLoadingClose,
  };
};
