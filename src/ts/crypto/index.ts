import CryptoES from "crypto-es";

/**
 * 将指定值转换为 MD5
 * @param message
 * @return string MD5 字符串 32 位的
 */
export function md5(message: string): string {
  return CryptoES.MD5(message).toString();
}

export function decode<T>(token: string): T | string {
  const parts = token.split(".");
  if (parts.length != 3) {
    return "给的数据不是 JWT";
  }
  const s = CryptoES.enc.Base64.parse(parts[1]).toString(CryptoES.enc.Latin1);
  try {
    return JSON.parse(s) as T;
  } catch (e) {
    return "payload 异常";
  }
}
