export interface AnyAny {
  [key: number]: any;
  [key: string]: any;
}

export interface NumberAny {
  [key: number]: any;
}

export type StrStr = Record<string, string>;

export type StrAny = Record<string, any>;

export type StrT<T> = Record<string, T>;

export type StrNumber = Record<string, number>;

export type StrStrAny = Record<string, StrAny>;

export type EmptyFunc = () => void;
