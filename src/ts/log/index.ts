import createDebug from "debug";

export const _d = createDebug;

export function initWindowDebug(): void {
  // @ts-ignore
  window._d = createDebug;
}

export function initWorkerDebug(): void {
  // @ts-ignore
  self._d = createDebug;
}

export function getLogger(name: string) {
  return createDebug(name);
}
