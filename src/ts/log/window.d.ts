import debug from "debug";

declare global {
  interface Window {
    _d: typeof debug;
  }
}
