export {};

declare class YsElectron {
  receive: (channel: string, listener: (...args: any[]) => void) => void;
  send: (channel: string, data: any) => void;
}

declare global {
  interface Window {
    isElectron?: boolean;
    ysElectron: YsElectron;
  }
}
