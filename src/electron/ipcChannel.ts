export const IPC_CHANNEL = {
  CLOSE_APP: "close-app",
  MIN_APP: "min-app",
  RESTORE_APP: "restore-app",
};
