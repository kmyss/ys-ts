import { computed, Ref, ref, ComputedRef } from "vue";
import { IPC_CHANNEL } from "./ipcChannel";

/**
 * 监听 MainWindows 大小改变时，是否为最大化
 * @param isMaximized 是否最大化
 */
function onMainRestore(isMaximized: Ref<boolean>) {
  window.ysElectron.receive(IPC_CHANNEL.RESTORE_APP, (data: boolean) => {
    isMaximized.value = data;
  });
}

/**
 * 窗口关闭
 */
function close() {
  window.ysElectron.send(IPC_CHANNEL.CLOSE_APP, null);
}

/**
 * 窗口还原或最大化
 */
function restore() {
  window.ysElectron.send(IPC_CHANNEL.RESTORE_APP, null);
}

/**
 * Electron 窗口最小化
 */
function min() {
  window.ysElectron.send(IPC_CHANNEL.MIN_APP, null);
}

export interface IUseElectron {
  // 是否在 Electron 运行
  isElectron: ComputedRef<boolean>;

  // 是否是最大化模式
  isMaximized: Ref<boolean>;

  // 窗口关闭
  close: () => void;

  // 窗口还原或最大化
  restore: () => void;

  // 窗口最小化
  min: () => void;
}

/**
 *  Electron 组合式API
 *  只能使用于 Vue3 的组合式API
 *
 *  @return IUseElectron
 */
export function useElectron(): IUseElectron {
  const isElectron = computed<boolean>(() => !!window.isElectron);
  const isMaximized = ref<boolean>(false);
  if (isElectron.value) {
    onMainRestore(isMaximized);
  }
  return {
    isElectron,
    isMaximized,
    close,
    restore,
    min,
  };
}
