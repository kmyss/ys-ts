import Two from "two.js";
import { DrawBaseClass } from "./Draw";

export interface ITwOptions {
  fullscreen?: boolean;
  fitted?: boolean;
  autostart?: boolean;
  width?: number;
  height?: number;
  type?: string;
  domElement?: HTMLElement;
  overdraw?: boolean;
  smoothing?: boolean;
}

export class TwoJSDrawCore extends DrawBaseClass {
  protected two!: Two;
  protected options?: ITwOptions;

  constructor(options: ITwOptions) {
    super();
    this.options = options;
  }

  init(el: HTMLElement): void {
    // 执行前置钩子
    super.init(el);
    this.two = new Two(this.options);
    this.two.appendTo(this.el);
  }

  update() {
    if (this.two) {
      this.two.update();
    }
  }

  dispose() {
    super.dispose();

    if (this.two) {
      this.two.clear();
      this.two.pause();
    }
  }

  /**
   * 获取 two.js 对象
   */
  twoInstance(): Two {
    return this.two;
  }
}
