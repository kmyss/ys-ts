import { DrawBaseClass } from "./Draw";
import * as monaco from "monaco-editor";

export class MonacoEditorCore extends DrawBaseClass<monaco.editor.ITextModel> {
  protected _editor: monaco.editor.IStandaloneCodeEditor | undefined =
    undefined;
  protected _options: monaco.editor.IStandaloneEditorConstructionOptions = {};

  constructor() {
    super();
    this.name = "monaco-editor";
  }

  /**
   * 设置当前的 options
   * @param options
   */
  setOptions(
    options: monaco.editor.IStandaloneEditorConstructionOptions
  ): void {
    this._options = options;
  }

  updateEditorOptions(
    options: monaco.editor.IEditorOptions & monaco.editor.IGlobalEditorOptions
  ) {
    this._editor?.updateOptions(options);
  }

  init(el: HTMLElement) {
    super.init(el);

    if (!this._editor) {
      this.newEditor();
    }
  }

  newEditor() {
    this._editor = monaco.editor.create(this.el, this._options);

    if (this.worker) {
      this._editor.setModel(this.worker);
    }
  }

  update() {
    if (this._editor) {
      const options = this._editor.getRawOptions();
      this._editor.dispose();
      this.newEditor();
      this.updateEditorOptions(options);
    }
  }

  draw() {
    try {
      this.getMustEditor().setModel(this.worker!);
    } catch (e) {
      this.logger("error");
    }
  }

  /**
   * 获取当前的 editor 实例
   *
   * 当不存在实例时报错
   */
  getMustEditor(): monaco.editor.IStandaloneCodeEditor {
    if (this._editor) {
      return this._editor;
    }

    throw Error("No editor");
  }

  setDataWorker(worker: monaco.editor.ITextModel) {
    super.setDataWorker(worker);
  }

  dispose() {
    super.dispose();
    if (this._editor) {
      this._editor.dispose();
    }
  }
}
