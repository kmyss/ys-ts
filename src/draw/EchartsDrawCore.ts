import { DrawBaseClass } from "./Draw";
import * as echarts from "echarts";
import { EChartsOption } from "echarts";

/**
 * 针对 Echart 的基础控制类
 */
export class EchartsDrawCore<T = any> extends DrawBaseClass<T> {
  protected chart?: echarts.ECharts;

  init(el: HTMLElement): void {
    // 执行前置钩子
    super.init(el);
    this.chart = echarts.init(el);
  }

  update() {
    if (this.chart) {
      this.logger(`${this.name} 重绘`);
      this.chart.resize();
      this.draw();
    }
  }

  draw() {
    if (!this.chart) {
      const err = "ehcart 未挂载";
      this.logger(err);
      throw Error(err);
    }

    if (this.worker == undefined) {
      const err = "数据载入";
      this.logger(err);
      throw Error(err);
    }
  }

  dispose() {
    super.dispose();

    if (this.chart) {
      this.chart.dispose();
    }
  }

  /**
   * 设置 echart 组件的 options
   *
   * 让外部程序任然可以部分控制 options 的设置过程
   *
   * @param optinos echarts option设置参数
   */
  public setOption(optinos: EChartsOption) {
    if (this.chart) {
      this.chart.setOption(optinos);
    }
  }

  /**
   * 获取当前 Draw 的 echarts 实例
   */
  public getEchartsInstance(): echarts.ECharts | undefined {
    return this.chart;
  }
}
