import { DrawBaseClass } from "./Draw";
import { isEmpty } from "../ts/lodash";
import mermaid from "mermaid";
import svgPanZoom from "svg-pan-zoom";

export class MermaidDrawCore extends DrawBaseClass<string> {
  private _id = "";
  private _width = "100%";
  private _pan: SvgPanZoom.Instance | null = null;
  private _svg: SVGElement | undefined;

  constructor() {
    super();
    this.name = "Mermaid";
  }

  set id(value: string) {
    this._id = value;
  }

  set width(value: string) {
    this._width = value;
  }

  get pan() {
    return this._pan;
  }

  toImage() {
    const image = new Image();
    const rect = this._svg!.getBoundingClientRect();
    image.width = rect.width;
    image.height = rect.height;
    image.src =
      "data:image/svg+xml;utf8," + encodeURIComponent(this._svg!.outerHTML);

    return new Promise<string>((resolve) => {
      image.onload = function () {
        const canvas = document.createElement("canvas");
        canvas.width = rect.width;
        canvas.height = rect.height;

        const context = canvas.getContext("2d");
        context.drawImage(image, 0, 0, rect.width, rect.height);

        resolve(canvas.toDataURL("image/png"));
      };
    });
  }

  save() {
    this.toImage().then((url: string) => {
      const a = document.createElement("a");
      a.href = url;
      a.download = "svg.png"; //设定下载名称
      a.click(); //点击触发下载
    });
  }

  init(el: HTMLElement) {
    super.init(el);

    this.el.id = isEmpty(this._id) ? "mermaid" : this._id;
  }

  update() {
    if (this.el) {
      this.draw();
    }
  }

  draw() {
    const svgId = "f" + this.el.id;
    mermaid.render(svgId, this.worker!, (svgCode: string) => {
      this.el.innerHTML = svgCode;
      this._svg = this.el.firstElementChild as SVGElement;

      this._svg.setAttribute("height", "100%");

      this._pan = svgPanZoom(this._svg, {
        fit: true,
        controlIconsEnabled: true,
      });
    });
  }
}
