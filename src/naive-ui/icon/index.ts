import {
  IconBack,
  IconDatabase,
  IconDelete,
  IconDismiss,
  IconDropdown,
  IconHome,
  IconInfo,
  IconPeople,
  IconRefresh,
  IconRole,
  IconEditConfig,
  IconContentConfig,
  IconPreview,
  IconPlay,
  IconPause,
  IconStop,
  IconReplay,
  IconPlayCircle,
  IconLed,
} from "./fluent";
import { h } from "vue";
import { NIcon } from "naive-ui";
export * from "./fluent";

export const iconMap: Map<string, any> = new Map<string, any>();

export function initIconMap() {
  iconMap.set("home", IconHome);
  iconMap.set("database", IconDatabase);
  iconMap.set("back", IconBack);
  iconMap.set("people", IconPeople);
  iconMap.set("role", IconRole);
  iconMap.set("dropdown", IconDropdown);
  iconMap.set("delete", IconDelete);
  iconMap.set("dismiss", IconDismiss);
  iconMap.set("info", IconInfo);
  iconMap.set("refresh", IconRefresh);
  iconMap.set("edit-config", IconEditConfig);
  iconMap.set("content-config", IconContentConfig);
  iconMap.set("preview", IconPreview);
  iconMap.set("play", IconPlay);
  iconMap.set("play-circle", IconPlayCircle);
  iconMap.set("replay", IconReplay);
  iconMap.set("pause", IconPause);
  iconMap.set("stop", IconStop);
  iconMap.set("led", IconLed);
}

export function icon(name: string): any {
  if (iconMap.has(name)) {
    return iconMap.get(name);
  } else {
    return null;
  }
}

export function renderIconByName(name: string) {
  const i = icon(name);
  if (i === null) {
    return () => h("p");
  } else {
    return renderIcon(i);
  }
}

export function renderIcon(icon: any) {
  return () => h(NIcon, null, { default: () => h(icon) });
}
