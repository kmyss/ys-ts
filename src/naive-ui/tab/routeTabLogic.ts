import { RouteLocationNormalized, RouteLocationRaw } from "vue-router";
import { defineStore } from "pinia";
import * as lodash from "lodash-es";
import { computed, unref, watch } from "vue";
import { IToNameOptions, useRouter } from "../router";
import { getLogger } from "../../ts/log";

const logger = getLogger("ys-ts.naive-ui.routetab");

export function newDefaultTab(): ITab {
  return { closeable: true, name: "default", label: "默认" };
}

export function newTabFromRoute(route: RouteLocationNormalized): ITab {
  const tab = newDefaultTab();
  tab.name = <string>route.name;
  tab.label = <string>route.name;
  tab.options = {
    query: route.query,
    params: route.params,
    hash: route.hash,
    state: history.state.ysTs,
  };
  tab.closeable = !route.meta.canNotCloseTab;

  return tab;
}

interface ITab {
  name: string; // 唯一标识
  label: string; // 显示的标签
  options?: IToNameOptions; // 需要带的参数
  closeable: boolean; // 是否显示关闭按钮
}

function findITabByName(name: string) {
  return function (value: ITab) {
    return value.name === name;
  };
}

interface store {
  // 记录要跳到的 route 名称
  to: string;
  // 当前选中的 Tab
  current: string;
  // 记录当前已有的 TAB
  tabs: ITab[];
}

export const useRouteTabStore = defineStore({
  // 它用于 devtools 并允许恢复状态
  id: "route-tab",
  state: (): store => ({
    current: "",
    tabs: [],
    to: "",
  }),
  actions: {
    findTabByName(name: string): ITab | undefined {
      return this.tabs.find(findITabByName(name));
    },
    findTabIndexByName(name: string): number {
      return this.tabs.findIndex(findITabByName(name));
    },
    addTabByRoute(route: RouteLocationNormalized): RouteLocationNormalized {
      const value = this.findTabByName(<string>route.name);

      logger("历史记录显示：", history.state);

      if (!value) {
        this.tabs.push(newTabFromRoute(route));
      } else {
        value.options = {
          query: route.query,
          params: route.params,
          hash: route.hash,
          state: lodash.cloneDeep(history.state.ysTs),
        };
      }

      return route;
    },
    removeTabByName(name: string) {
      const value = this.findTabIndexByName(name);
      if (value !== -1) {
        this.tabs.splice(value, 1);
      }
      // 删除当前 tab 时换前一个页面
      if (name === this.current) {
        this.current = this.tabs[Math.min(value, this.tabs.length - 1)].name;
      }
    },
  },
});

/**
 * 在路由跳转前处理 TAB 的存储问题
 * 无法存储通过 history state 存储的值
 * @param to
 * @deprecated
 */
export function selectOrAddTab(
  to: RouteLocationNormalized
): RouteLocationRaw | boolean {
  logger(`tab 切换前处理`);

  const store = useRouteTabStore();

  store.to = <string>to.name;

  if (to.meta.useTab) {
    if (store.current !== <string>to.name) {
      store.current = <string>to.name;
      store.addTabByRoute(to);
    }
  } else {
    store.current = "";
  }
  logger("处理完成");
  return true;
}

/**
 * 路由跳转后处理 tab 标签的显示
 * @param to
 */
export function saveStateAfterEach(to: RouteLocationNormalized) {
  logger(`tab 切换后处理`);
  const store = useRouteTabStore();
  store.to = <string>to.name;
  if (to.meta.useTab) {
    // 处理通过路由直接跳转时current 值不正确问题
    if (store.current !== <string>to.name) {
      store.current = <string>to.name;
    }
    // 将跳转的路由加入 tab 中
    store.addTabByRoute(to);
    logger(`显示标签页`);
  } else {
    store.current = "";
    logger(`不显示标签`);
  }
  logger("处理完成");
  return;
}

/**
 * 当前选择的页面 Tab 改变后的处理事件
 */
export function useRouteTabChange() {
  const store = useRouteTabStore();
  const { toNameAll } = useRouter();

  const current = computed<string>(() => {
    return store.current;
  });

  watch(current, function () {
    logger(
      `监测到 current 修改! 当前值：${current.value} 跳转值：${store.to}`,
      current.value,
      store.to
    );

    if (store.to !== current.value && current.value != "") {
      const tab = store.findTabByName(current.value);
      if (tab) {
        toNameAll(tab.name, lodash.cloneDeep(tab.options));
      } else {
        toNameAll(current.value);
      }
    }
  });

  const closeHandler = (name: string) => {
    store.removeTabByName(name);
  };

  return {
    closeHandler,
  };
}
