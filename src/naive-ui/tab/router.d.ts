import "vue-router";

declare module "vue-router" {
  interface RouteMeta {
    useTab?: boolean; // 判断当前路由是否要进行 tab 检查
    canNotCloseTab?: boolean; // 判断是否可以被关闭
  }
}
