import {
  HistoryState,
  LocationQueryRaw,
  RouteLocationNormalized,
  RouteParams,
  RouteQueryAndHash,
  useRoute,
  useRouter as baseRouter,
} from "vue-router";
import { useLoadingBar } from "naive-ui";
import { useI18n, UseI18nOptions } from "vue-i18n";
import { getLogger } from "../../ts/log";
import { isEmpty } from "../../ts/lodash";

export interface IToNameOptions {
  query?: LocationQueryRaw;
  hash?: string;
  params?: RouteParams;
  state?: HistoryState;
}
const logger = getLogger("ys-ts.naive-ui.router");
export function useRouter<Options extends UseI18nOptions = UseI18nOptions>() {
  const router = baseRouter();
  const route = useRoute();
  const loadingBar = useLoadingBar();

  /**
   * 使用 Router 标准参数增加路由配置
   * 可以将部分参数通过 query 传递从而实现存储历史的后退功能
   * @param name
   * @param options
   */
  async function toNameAll(name: string, options: IToNameOptions = {}) {
    logger(`ToNameAll ${name}`, options);
    loadingBar.start();
    if (router.hasRoute(name)) {
      // 对 state 存储到 固定的 ysTs 下
      const _opt = {
        ...options,
      };
      if (options.state) {
        _opt.state = { ysTs: options.state };
      }

      await router.push({
        name: name,
        ..._opt,
      });
    } else {
      logger(`没有找到 name`);
      await router.push({
        path: `/${name}`,
        ...options,
      });
    }
    loadingBar.finish();
  }

  async function toName(name: string, params?: RouteParams) {
    await toNameAll(name, { params: params });
  }

  const { t } = useI18n<Options>();
  function translate(name: string) {
    return t(`router.${name}`);
  }

  return {
    router,
    route,
    toName,
    toNameAll,
    translate,
  };
}

export function defaultProps(route: RouteLocationNormalized): any {
  logger("route", route);
  if (!isEmpty(route.query)) {
    logger("读取 query");
    return route.query;
  }
  if (!isEmpty(route.params)) {
    logger("读取 params");
    return route.params;
  }
  if (!isEmpty(history.state.ysTs)) {
    logger("history", history.state);
    logger("读取 history.state");
    return history.state.ysTs;
  }
  return {};
}
