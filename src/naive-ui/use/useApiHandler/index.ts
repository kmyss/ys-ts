import { useMessage } from "naive-ui";
import { IResponseData } from "../../../ts/api";

/**
 * 基于 Naive-UI 的 API 统一处理方法
 */
export function useApiHandler() {
  const message = useMessage();

  /**
   * 故障信息处理
   * @param err API 返回的故障信息
   * @param isMessage 是否使用 message 显示
   */
  function errorHandler(err: IResponseData<null> | any, isMessage = false) {
    if (err.message && err.code) {
      const msg = `${err.message}(错误码: ${err.code})`;

      if (isMessage) {
        message.error(msg);
      } else {
        console.error(msg);
      }
    } else {
      console.error(err);
    }
  }

  return { errorHandler };
}
