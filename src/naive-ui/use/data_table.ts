import { computed, Ref, ref } from "vue";

export function useDataTableHandler<T = number>() {
  const selected = ref([]) as Ref<T[]>;
  const hasOneSelected = computed(() => {
    return selected.value.length === 1;
  });

  const hasSelected = computed(() => {
    return selected.value.length !== 0;
  });

  return {
    selected,
    hasOneSelected,
    hasSelected,
  };
}
