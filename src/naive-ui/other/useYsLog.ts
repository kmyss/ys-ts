import { default as h } from "highlight.js/es/core";
import "./useYsLog.css";

export const useYsLog = (hljs: typeof h | null = null) => {
  if (hljs === null) {
    hljs = h;
  }

  // 注册语言高亮
  hljs.registerLanguage("ys-log", () => ({
    contains: [
      {
        scope: "yslog.time",
        match:
          /[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}[,.][0-9]{3}/,
      },
      {
        className: "yslog.info",
        match: /\[INFO\]/,
      },
      {
        className: "yslog.debug",
        match: /\[(DEBU|DEBUG)\]/,
      },
      {
        className: "yslog.warn",
        match: /\[(WARN|WARNING)\]/,
      },
      {
        className: "yslog.error",
        match: /\[(ERROR|ERRO)\]/,
      },
      {
        className: "yslog.model",
        match: /\[[a-zA-Z-\.]+\]/,
      },
    ],
  }));

  return { hljs };
};
