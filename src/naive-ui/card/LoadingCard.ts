import { computed, ref } from "vue";
import { useLoading } from "../../vue/use/useLoading";

export function useLoadingCardStyle(first = true, loadingTime = 1000) {
  const { loading, delayLoadingClose } = useLoading(loadingTime);
  loading.value = first;
  const contentStyle = computed(() => {
    return {
      padding: loading.value ? 0 : undefined,
    };
  });

  return {
    showLoading: loading,
    contentStyle,
    delayLoadingClose,
  };
}
