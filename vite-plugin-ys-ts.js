module.exports = () => ({
  name: "ys-ts",
  config: () => ({
    optimizeDeps: {
      include: [
        "highlight.js/es/core",
        "path-browserify",
        "mermaid",
        "monaco-editor",
        "svg-pan-zoom",
        "debug",
        "axios",
        "lodash",
        "yaml",
        "events",
        "lodash",
        "@vicons/fluent",
        "pinia",
        "naive-ui",
        "vue-i18n",
        "vue-router",
        "vue",
      ],
      exclude: ["ys-ts"],
    },
  }),
});
