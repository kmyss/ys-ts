/** @type {import('tailwindcss').Config} */
module.exports = {
  prefix: "tw-",
  important: true,
  theme: {
    extend: {
      spacing: {
        136: "34rem",
        15: "3.75rem",
        22: "5.5rem",
        200: "50rem",
        "50vh": "50vh",
        "60vh": "60vh",
        "70vh": "70vh",
        "80vh": "80vh",
        "114px": "114px",
        "62px": "62px",
        "52px": "52px",
        "1440px": "1440px",
        "1220px": "1220px",
      },
      colors: {
        noneColor: "rgba(0,0,0,0)",
        "blue-cyan": "#00D7FF",
        "node-no-run": "#a6a6a6",
        "node-running": "#ffd700",
        "node-complete": "#0ac55a",
        "node-error": "#ff7d00",
        primary: {
          default: "var(--color-primary)",
          hover: "var(--color-primary-hover)",
          pressed: "var(--color-primary-pressed)",
          suppl: "var(--color-primary-suppl)",
        },
        info: {
          default: "var(--color-info)",
          hover: "var(--color-info-hover)",
          pressed: "var(--color-info-pressed)",
          suppl: "var(--color-info-suppl)",
        },
        success: {
          default: "var(--color-success)",
          hover: "var(--color-success-hover)",
          pressed: "var(--color-success-pressed)",
          suppl: "var(--color-success-suppl)",
        },
        warning: {
          default: "var(--color-warning)",
          hover: "var(--color-warning-hover)",
          pressed: "var(--color-warning-pressed)",
          suppl: "var(--color-warning-suppl)",
        },
        error: {
          default: "var(--color-error)",
          hover: "var(--color-error-hover)",
          pressed: "var(--color-error-pressed)",
          suppl: "var(--color-error-suppl)",
        },
      },
      maxHeight: {
        "without-h": "calc(100vh - 64px)", // header padding
        "without-hp": "calc(100vh - 64px - 87px)", // header padding
        "without-hpl": "calc(100vh - 64px - 87px - 42px)", // header padding link
      },
      minWidth: {
        "1280px": "1280px",
      },
      minHeight: {
        "without-h": "calc(100vh - 4rem)", // header padding
      },
      transitionProperty: {
        rotate: "rotate",
      },
      keyframes: {
        led: {
          "0%, 100%": {},
          "50%": { color: "#ffffff", opacity: 0.3 },
        },
      },
      animation: {
        led: "led 1.5s cubic-bezier(0.68, -0.6, 0.32, 1.6) infinite",
        "pulse-slow": "pulse 1.5s cubic-bezier(0.4, 0, 0.6, 1) infinite;",
      },
    },
  },
  corePlugins: {
    preflight: false,
  },
  plugins: [],
};
